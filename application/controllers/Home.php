<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('user/home');
		$this->load->view('template/footer');
	}
}