<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model("menu_model");
        $this->load->library('form_validation');
    }

	public function index()
	{
		$data['menu'] = $this->menu_model->getAll();
		$this->load->view('template/header');
		$this->load->view('menu/index',$data);
		$this->load->view('template/footer');
	}

	public function create()
	{
		$this->load->view('template/header');
		$this->load->view('menu/create');
		$this->load->view('template/footer');
	}
    public function save()
	{
		$this->form_validation->set_rules('nama_menu','Nama Menu','required');
        $this->form_validation->set_rules('harga','Harga','required');
		$this->form_validation->set_rules('keterangan','Keterangan','required');
		if ($this->form_validation->run()==true)
        {
			$data['nama_menu'] = $this->input->post('nama_menu');
            $data['harga'] = $this->input->post('harga');
			$data['keterangan'] = $this->input->post('keterangan');
			$this->menu_model->save($data);
			redirect('menu');
		}
		else
		{
			$this->load->view('template/header');
			$this->load->view('menu/create');
			$this->load->view('template/footer');
		}
	}

	function edit($id_menu)
	{
		$data['menu'] = $this->menu_model->getById($id_menu);
		$this->load->view('template/header');
		$this->load->view('menu/edit',$data);
		$this->load->view('template/footer');
	}

	public function update()
	{
		$this->form_validation->set_rules('nama_menu','Nama Menu','required');
        $this->form_validation->set_rules('harga','Harga','required');
		$this->form_validation->set_rules('keterangan','Keterangan','required');
		if ($this->form_validation->run()==true)
        {
		 	$id_menu = $this->input->post('id_menu');
             $data['nama_menu'] = $this->input->post('nama_menu');
             $data['harga'] = $this->input->post('harga');
             $data['keterangan'] = $this->input->post('keterangan');
			$this->menu_model->update($data,$id_menu);
			redirect('menu');
		}
		else
		{
			$id_menu = $this->input->post('id_menu');
			$data['menu'] = $this->menu_model->getById($id_menu);
			$this->load->view('template/header');
			$this->load->view('menu/edit',$data);
			$this->load->view('template/footer');
		}
	}

	function delete($id_menu)
	{
		$this->menu_model->delete($id_menu);
		redirect('menu');
	}

}
