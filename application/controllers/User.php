<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model("user_model");
        $this->load->library('form_validation');
    }

	public function index()
	{
		$data['user'] = $this->user_model->getAll();
		$this->load->view('template/header');
		$this->load->view('user/index',$data);
		$this->load->view('template/footer');
	}

	public function home()
	{
		$this->load->view('template/header');
		$this->load->view('user/home');
		$this->load->view('template/footer');
	}

    public function create()
	{
		$this->load->view('template/header');
		$this->load->view('user/create');
		$this->load->view('template/footer');
	}

	public function save()
	{
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('level','Level','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('no_telp','Nomor Telepon','required');
		if ($this->form_validation->run()==true)
        {
			$data['nama'] = $this->input->post('nama');
			$data['level'] = $this->input->post('level');
            $data['alamat'] = $this->input->post('alamat');
			$data['no_telp'] = $this->input->post('no_telp');
			$this->user_model->save($data);
			redirect('user');
		}
		else
		{
			$this->load->view('template/header');
			$this->load->view('user/create');
			$this->load->view('template/footer');
		}
	}

	function edit($id_user)
	{
		$data['user'] = $this->user_model->getById($id_user);
		$this->load->view('template/header');
		$this->load->view('user/edit',$data);
		$this->load->view('template/footer');
	}

	public function update()
	{
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('level','Level','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('no_telp','Nomor Telepon','required');
		if ($this->form_validation->run()==true)
        {
		 	$id_user = $this->input->post('id_user');
             $data['nama'] = $this->input->post('nama');
             $data['level'] = $this->input->post('level');
             $data['alamat'] = $this->input->post('alamat');
             $data['no_telp'] = $this->input->post('no_telp');
			$this->user_model->update($data,$id_user);
			redirect('user');
		}
		else
		{
			$id_user = $this->input->post('id_user');
			$data['user'] = $this->user_model->getById($id_user);
			$this->load->view('template/header');
			$this->load->view('user/edit',$data);
			$this->load->view('template/footer');
		}
	}

	function delete($id_user)
	{
		$this->user_model->delete($id_user);
		redirect('user');
	}


}
