<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model("pelanggan_model");
        $this->load->library('form_validation');
    }

	public function index()
	{
		$data['pelanggan'] = $this->pelanggan_model->getAll();
		$this->load->view('template/header');
		$this->load->view('pelanggan/index',$data);
		$this->load->view('template/footer');
	}

	public function create()
	{
		$this->load->view('template/header');
		$this->load->view('pelanggan/create');
		$this->load->view('template/footer');
	}

	public function save()
	{
		$this->form_validation->set_rules('nama_pelanggan','Nama Pelanggan','required');
		$this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('no_telp','Nomor Telepon','required');
		if ($this->form_validation->run()==true)
        {
			$data['nama_pelanggan'] = $this->input->post('nama_pelanggan');
			$data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
            $data['alamat'] = $this->input->post('alamat');
			$data['no_telp'] = $this->input->post('no_telp');
			$this->pelanggan_model->save($data);
			redirect('pelanggan');
		}
		else
		{
			$this->load->view('template/header');
			$this->load->view('pelanggan/create');
			$this->load->view('template/footer');
		}
	}

	function edit($id_pelanggan)
	{
		$data['pelanggan'] = $this->pelanggan_model->getById($id_pelanggan);
		$this->load->view('template/header');
		$this->load->view('pelanggan/edit',$data);
		$this->load->view('template/footer');
	}

	public function update()
	{
		$this->form_validation->set_rules('nama_pelanggan','Nama Pelanggan','required');
		$this->form_validation->set_rules('jenis_kelamin','Jenis Kelamin','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('no_telp','Nomor Telepon','required');
		if ($this->form_validation->run()==true)
        {
		 	$id_pelanggan = $this->input->post('id_pelanggan');
             $data['nama_pelanggan'] = $this->input->post('nama_pelanggan');
             $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
             $data['alamat'] = $this->input->post('alamat');
             $data['no_telp'] = $this->input->post('no_telp');
			$this->pelanggan_model->update($data,$id_pelanggan);
			redirect('pelanggan');
		}
		else
		{
			$id_pelanggan = $this->input->post('id_pelanggan');
			$data['pelanggan'] = $this->pelanggan_model->getById($id_pelanggan);
			$this->load->view('template/header');
			$this->load->view('pelanggan/edit',$data);
			$this->load->view('template/footer');
		}
	}

	function delete($id_pelanggan)
	{
		$this->pelanggan_model->delete($id_pelanggan);
		redirect('pelanggan');
	}

}