<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model("product_model");
        $this->load->library('form_validation');
    }

	public function requestProducts()
	{
		$data['products'] = $this->products_model->getProduct();
        $this->load->view('template/header');
		$this->load->view('products/index',$data);
        $this->load->view('template/footer');
	}

    public function create()
	{
		$this->load->view('template/header');
		$this->load->view('products/create');
		$this->load->view('template/footer');
	}

    public function save()
	{
		$this->form_validation->set_rules('id_product','Id Product','required');
        $this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('price','Price','required');
		if ($this->form_validation->run()==true)
        {
			$data['id_product'] = $this->input->post('id_product');
            $data['name'] = $this->input->post('name');
			$data['price'] = $this->input->post('price');
			$this->products_model->save($data);
			redirect('products');
		}
		else
		{
			$this->load->view('template/header');
			$this->load->view('products/create');
			$this->load->view('template/footer');
		}
	}

    function edit($id_product)
	{
		$data['products'] = $this->product_model->getById($id_product);
		$this->load->view('template/header');
		$this->load->view('product/edit',$data);
		$this->load->view('template/footer');
	}


	public function update()
	{
		$this->form_validation->set_rules('id_product','Id Product','required');
        $this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('price','Price','required');
		if ($this->form_validation->run()==true)
        {
		 	$id_product = $this->input->post('id_product');
            $data['name'] = $this->input->post('name');
			$data['price'] = $this->input->post('price');
			$this->products_model->update($data,$id_products);
			redirect('products');
		}
		else
		{
			$id_product = $this->input->post('id_product');
			$data['products'] = $this->products_model->getById($id_product);
			$this->load->view('template/header');
			$this->load->view('product/edit',$data);
			$this->load->view('template/footer');
		}
	}

	function delete($id_product)
	{
		$this->products_model->delete($id_product);
		redirect('products');
	}

}
