<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model("pesanan_model");
        $this->load->library('form_validation');
    }

	public function index()
	{
		$data['pesanan'] = $this->pesanan_model->getAll();
		$this->load->view('template/header');
		$this->load->view('pesanan/index',$data);
		$this->load->view('template/footer');
	}


	public function create()
	{
		$this->load->view('template/header');
		$this->load->view('pesanan/create');
		$this->load->view('template/footer');
	}

    public function save()
	{
		$this->form_validation->set_rules('id_pelanggan','Id Pelanggan','required');
        $this->form_validation->set_rules('id_user','Id User','required');
        $this->form_validation->set_rules('id_menu','Id Menu','required');
		$this->form_validation->set_rules('jumlah_pesanan','Jumlah Pesanan','required');
		$this->form_validation->set_rules('total','Total','required');
		if ($this->form_validation->run()==true)
        {
			$data['id_pelanggan'] = $this->input->post('id_pelanggan');
			$data['id_user'] = $this->input->post('id_user');
			$data['id_menu'] = $this->input->post('id_menu');
			$data['jumlah_pesanan'] = $this->input->post('jumlah_pesanan');
			$data['total'] = $this->input->post('total');
			$this->pesanan_model->save($data);
			redirect('pesanan');
		}
		else
		{
			$this->load->view('template/header');
			$this->load->view('pesanan/create');
			$this->load->view('template/footer');
		}
	}

	function edit($id_pesanan)
	{
		$data['pesanan'] = $this->pesanan_model->getById($id_pesanan);
		$this->load->view('template/header');
		$this->load->view('pesanan/edit',$data);
		$this->load->view('template/footer');
	}

	public function update()
	{
		$this->form_validation->set_rules('id_pelanggan','Id Pelanggan','required');
        $this->form_validation->set_rules('id_user','Id User','required');
        $this->form_validation->set_rules('id_menu','Id Menu','required');
		$this->form_validation->set_rules('jumlah_pesanan','Jumlah Pesanan','required');
		$this->form_validation->set_rules('total','Total','required');
		if ($this->form_validation->run()==true)
        {
		 	$id_pesanan = $this->input->post('id_pesanan');
             $data['id_pelanggan'] = $this->input->post('id_pelanggan');
             $data['id_user'] = $this->input->post('id_user');
             $data['id_menu'] = $this->input->post('id_menu');
             $data['jumlah_pesanan'] = $this->input->post('jumlah_pesanan');
             $data['total'] = $this->input->post('total');
			$this->pesanan_model->update($data,$id_pesanan);
			redirect('pesanan');
		}
		else
		{
			$id_pesanan = $this->input->post('id_pesanan');
			$data['pesanan'] = $this->pesanan_model->getById($id_pesanan);
			$this->load->view('template/header');
			$this->load->view('pesanan/edit',$data);
			$this->load->view('template/footer');
		}
	}

	function delete($id_pesanan)
	{
		$this->pesanan_model->delete($id_pesanan);
		redirect('pesanan');
	}

    function editpembayaran($id_pesanan)
	{
		$data['pesanan'] = $this->pesanan_model->getById($id_pesanan);
		$this->load->view('template/header');
		$this->load->view('pesanan/edit-pembayaran',$data);
		$this->load->view('template/footer');
	}

	public function updatepembayaran()
	{
		$this->form_validation->set_rules('id_pelanggan','Id Pelanggan','required');
		$this->form_validation->set_rules('total','Total','required');
        $this->form_validation->set_rules('status','Status','required');
		if ($this->form_validation->run()==true)
        {
		 	$id_pesanan = $this->input->post('id_pesanan');
             $data['id_pelanggan'] = $this->input->post('id_pelanggan');
             $data['total'] = $this->input->post('total');
             $data['status'] = $this->input->post('status');
			$this->pesanan_model->update($data,$id_pesanan);
			redirect('pesanan');
		}
		else
		{
			$id_pesanan = $this->input->post('id_pesanan');
			$data['pesanan'] = $this->pesanan_model->getById($id_pesanan);
			$this->load->view('template/header');
			$this->load->view('pesanan/edit-pembayaran',$data);
			$this->load->view('template/footer');
		}
	}


}

