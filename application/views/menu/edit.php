<div>
		<div class="card">
			<div class="card-body">
			<div class="container" style="text-align:center;"><h4><b>Edit Menu</b></h4></div></br>
			<?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?>
			<form method="post" action="<?php echo base_url(); ?>menu/update">
            <input type="hidden" name="id_menu" id="id_menu" value="<?php echo $menu->id_menu; ?>"/>
				<div class="form-group">
					<label for="nama">Nama Menu</label>
					<input type="text" value="<?php echo $menu->nama_menu; ?>" class="form-control" id="nama_menu" name="nama_menu">
				</div>

                <div class="form-group">
					<label for="harga">Harga</label>
					<input type="text" class="form-control" id="harga" name="harga" value="<?php echo $menu->harga; ?>">
				</div>

				<div class="form-group">
					<label for="keterangan">Keterangan</label>
					<select name="keterangan" id="keterangan" class="form-control">
						<option value="tersedia" <?php echo ($menu->keterangan ? 'tersedia' : 'selected' ); ?> >Tersedia</option>
						<option value="tidak" <?php echo ($menu->tidak ? 'tidak' : 'selected' ); ?>>Tidak</option>
					</select>
				</div>


				<button type="submit" class="btn btn-primary">Update</button>
                <a href="<?php echo base_url(); ?>menu/index" class="btn btn-success">Batal</a>
			</form>
			<div class="container" style="text-align:center;">
        		<hr></hr>
       				 <span>Aplikasi Penjualan UMKM Apem Kesesi <?php echo date('Y'); ?></span>
       			 <br></br>
     		</div>
			</div>
		</div>
	</div>

