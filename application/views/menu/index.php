<main role="main">
		<div class="card">
			<div class="card-body">
			<div class="container" style="text-align:center;"><h4><b>Data Menu</b></h4></div></br>
				<a href="<?php echo base_url(); ?>menu/create" class="btn btn-success">Create</a>
				<br/>
				<br/>
				<table class="table table-bordered">
					<tr>
                        <th>Id Menu</th>
						<th>Nama Menu</th>
						<th>Harga</th>
						<th>Keterangan</th>
						<th>Action</th>
					</tr>
					<?php 
					$no = 1;
					foreach($menu as $row)
					{
						?>
						<tr>
							<td><?php echo $row->id_menu; ?></td>
							<td><?php echo $row->nama_menu; ?></td>
							<td><?php echo $row->harga; ?></td>
							<td><?php echo $row->keterangan; ?></td>
							<td>
							<a href="<?php echo base_url(); ?>menu/edit/<?php echo $row->id_menu; ?>" class="btn btn-warning">Edit</a>
							<a href="<?php echo base_url(); ?>menu/delete/<?php echo $row->id_menu; ?>" class="btn btn-danger">Hapus</a>
							</td>
						</tr>
						<?php
					}
					?>
				</table>
                <div class="container" style="text-align:center;">
        		<hr></hr>
       				 <span>Aplikasi Penjualan UMKM Apem Kesesi <?php echo date('Y'); ?></span>
       			 <br></br>
     			</div>
			</div>
		</div>
</main>
