<div>
		<div class="card">
			<div class="card-body">
			<div class="container" style="text-align:center;"><h4><b>Create Menu</b></h4></div></br>
			<?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?>
			<form method="post" action="<?php echo base_url(); ?>menu/save">
				<div class="form-group">
					<label for="nama_menu">Nama Menu</label>
					<input type="text" class="form-control" id="nama_menu" name="nama_menu">
				</div>

                <div class="form-group">
					<label for="harga">Harga</label>
					<input type="text" class="form-control" id="harga" name="harga">
				</div>

				<div class="form-group">
					<label for="keterangan">Keterangan</label>
					<select name="keterangan" id="keterangan" class="form-control">
						<option value="tersedia">Tersedia</option>
						<option value="tidak">Tidak</option>
					</select>
				</div>

<!--				<div class="form-group">
					<label for="no_telp">No Telp</label>
					<input type="number" class="form-control" id="no_telp" name="no_telp">
				</div> -->

				<button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo base_url(); ?>menu/index" class="btn btn-success">Batal</a>
			</form>
			<div class="container" style="text-align:center;">
        		<hr></hr>
       				 <span>Aplikasi Penjualan UMKM Apem Kesesi <?php echo date('Y'); ?></span>
       			 <br></br>
     			</div>
			</div>
			</div>
		</div>
	</div>

