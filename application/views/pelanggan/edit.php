<div>
		<div class="card">
			<div class="card-body">
			<div class="container" style="text-align:center;"><h4><b>Edit Pelanggan</b></h4></div></br>
			<?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?>
			<form method="post" action="<?php echo base_url(); ?>pelanggan/update">
				<input type="hidden" name="id_pelanggan" id="id_pelanggan" value="<?php echo $pelanggan->id_pelanggan; ?>"/>
				<div class="form-group">
					<label for="nama">Nama Pelanggan</label>
					<input type="text" value="<?php echo $pelanggan->nama_pelanggan; ?>" class="form-control" id="nama_pelanggan" name="nama_pelanggan">
				</div>

				<div class="form-group">
					<label for="jenis_kelamin">Jenis Kelamin</label>
					<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
						<option value="Laki-laki" <?php echo ($pelanggan->jenis_kelamin ? 'Laki-laki' : 'selected' ); ?> >Laki-laki</option>
						<option value="Perempuan" <?php echo ($pelanggan->jenis_kelamin ? 'Perempuan' : 'selected' ); ?>>Perempuan</option>
					</select>
				</div>

				<div class="form-group">
					<label for="alamat">Alamat</label>
					<textarea class="form-control" name="alamat" id="alamat"><?php echo $pelanggan->alamat; ?></textarea>
				</div>

				<div class="form-group">
					<label for="no_telp">No Telp</label>
					<input type="number" class="form-control" value="<?php echo $pelanggan->no_telp; ?>" id="no_telp" name="no_telp">
				</div>

				<button type="submit" class="btn btn-primary">Update</button>
                <a href="<?php echo base_url(); ?>pelanggan/index" class="btn btn-success">Batal</a>
			</form>
			<div class="container" style="text-align:center;">
        		<hr></hr>
       				 <span>Aplikasi Penjualan UMKM Apem Kesesi <?php echo date('Y'); ?></span>
       			 <br></br>
     			</div>
			</div>
		</div>
	</div>

