
<main role="main" >
		<div class="card">
			<div class="card-body">
				<div class="container" style="text-align:center;"><h4><b>Data User</b></h4></div></br>
				<a href="<?php echo base_url(); ?>user/create" class="btn btn-success">Create</a>
				<br/>
				<br/>
				<table class="table table-bordered">
					<tr>
                        <th>Id User</th>
						<th>Nama</th>
						<th>Level</th>
						<th>Alamat</th>
                        <th>Telp</th>
						<th>Action</th>
					</tr>
					<?php 
					$no = 1;
					foreach($user as $row)
					{
						?>
						<tr>
						    <td><?php echo $row->id_user; ?></td>	
                            <td><?php echo $row->nama; ?></td>
							<td><?php echo $row->level; ?></td>
							<td><?php echo $row->alamat; ?></td>
                            <td><?php echo $row->no_telp; ?></td>
							<td>
							<a href="<?php echo base_url(); ?>user/edit/<?php echo $row->id_user; ?>" class="btn btn-warning">Edit</a>
							<a href="<?php echo base_url(); ?>user/delete/<?php echo $row->id_user; ?>" class="btn btn-danger">Hapus</a>
							</td>
						</tr>
						<?php
					}
					?>
				</table>
				<div class="container" style="text-align:center;">
        		<hr></hr>
       				 <span>Aplikasi Penjualan UMKM Apem Kesesi <?php echo date('Y'); ?></span>
       			 <br></br>
     			</div>
			</div>
		</div>
</main>
