
<div >
		<div class="card">
			<div class="card-body">
			<div class="container" style="text-align:center;"><h4><b>Edit User</b></h4></div></br>
			<?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?>
			<form method="post" action="<?php echo base_url(); ?>user/update">
				<input type="hidden" name="id_user" id="id_user" value="<?php echo $user->id_user; ?>"/>
				

                <div class="form-group">
					<label for="nama">Nama</label>
					<input type="text" value="<?php echo $user->nama; ?>" class="form-control" id="nama" name="nama">
				</div>

				<div class="form-group">
					<label for="level">Level</label>
					<select name="level" id="level" class="form-control">
						<option value="pemilik" <?php echo ($user->level ? 'pemilik' : 'selected' ); ?> >Pemilik</option>
						<option value="karyawan" <?php echo ($user->level ? 'karyawan' : 'selected' ); ?>>Karyawan</option>
					</select>
				</div>

                <div class="form-group">
					<label for="alamat">Alamat</label>
					<textarea class="form-control" name="alamat" id="alamat"><?php echo $user->alamat; ?></textarea>
				</div>

				<div class="form-group">
					<label for="no_telp">No Telp</label>
					<input type="text" class="form-control" value="<?php echo $user->no_telp; ?>" id="no_telp" name="no_telp">
				</div>


				<button type="submit" class="btn btn-primary">Update</button>
                <a href="<?php echo base_url(); ?>user/index" class="btn btn-success">Batal</a>
			</form>
			<div class="container" style="text-align:center;">
        		<hr></hr>
       				 <span>Aplikasi Penjualan UMKM Apem Kesesi <?php echo date('Y'); ?></span>
       			 <br></br>
     		</div>
			</div>
		</div>
	</div>

