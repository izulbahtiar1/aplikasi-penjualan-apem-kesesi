<div>
		<div class="card">
			<div class="card-body">
			<div class="container" style="text-align:center;"><h4><b>Create Pesanan</b></h4></div></br>
			<?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?>
			<form method="post" action="<?php echo base_url(); ?>pesanan/save">
				<div class="form-group">
					<label for="id_pelanggan">Id Pelanggan</label>
					<input type="text" class="form-control" id="id_pelanggan" name="id_pelanggan">
				</div>

                <div class="form-group">
					<label for="id_user">Id User</label>
					<input type="text" class="form-control" id="id_user" name="id_user">
				</div>

                <div class="form-group">
					<label for="id_menu">Id Menu</label>
					<input type="text" class="form-control" id="id_menu" name="id_menu">
				</div>


				<div class="form-group">
					<label for="jumlah_pesanan">Jumlah Pesanan</label>
					<input type="text" class="form-control" id="jumlah_pesanan" name="jumlah_pesanan">
				</div>

				<div class="form-group">
					<label for="total">Total</label>
					<input type="text" class="form-control datepicker" id="total" name="total">
				</div>

				<button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo base_url(); ?>pesanan/index" class="btn btn-success">Batal</a>
			</form>
			<div class="container" style="text-align:center;">
        		<hr></hr>
       				 <span>Aplikasi Penjualan UMKM Apem Kesesi <?php echo date('Y'); ?></span>
       			 <br></br>
     			</div>
			</div>
			</div>
		</div>
	</div>