<main role="main">
		<div class="card">
			<div class="card-body">
				<div class="container" style="text-align:center;"><h4><b>Data Pesanan</b></h4></div></br>
				<a href="<?php echo base_url(); ?>pesanan/create" class="btn btn-success">Create</a>
				<br/>
				<br/>
				<table class="table table-bordered">
					<tr>
						<th>Id Pesanan</th>
						<th>Id User</th>
                        <th>Id Pelanggan</th>
                        <th>Id Menu</th>
                        <th>Jumlah</th>
                        <th>Total</th>
						<th>Tanggal</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					<?php 
					$no = 1;
					foreach($pesanan as $row)
					{
						?>
						<tr>
							<td><?php echo $row->id_pesanan; ?></td>
							<td><?php echo $row->id_user; ?></td>
                            <td><?php echo $row->id_pelanggan; ?></td>
                            <td><?php echo $row->id_menu; ?></td>
                            <td><?php echo $row->jumlah_pesanan; ?></td>
                            <td><?php echo $row->total; ?></td>
							<td><?php echo $row->tanggal; ?></td>
							<td><?php echo $row->status; ?></td>
							<td>
							<a href="<?php echo base_url(); ?>pesanan/edit/<?php echo $row->id_pesanan; ?>" class="btn btn-warning">Edit</a>
                            <a href="<?php echo base_url(); ?>pesanan/editpembayaran/<?php echo $row->id_pesanan; ?>" class="btn btn-primary">Bayar</a>
							<a href="<?php echo base_url(); ?>pesanan/delete/<?php echo $row->id_pesanan; ?>" class="btn btn-danger">Hapus</a>
							</td>
						</tr>
						<?php
					}
					?>
				</table>
				<div class="container" style="text-align:center;">
        		<hr></hr>
       				 <span>Aplikasi Penjualan UMKM Apem Kesesi <?php echo date('Y'); ?></span>
       			 <br></br>
     			</div>
			</div>
		</div>
</main>
