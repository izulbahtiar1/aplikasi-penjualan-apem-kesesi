<div>
		<div class="card">
			<div class="card-body">
			<div class="container" style="text-align:center;"><h4><b>Edit Pembayaran</b></h4></div></br>
			<?php 
			if(validation_errors() != false)
			{
				?>
				<div class="alert alert-danger" role="alert">
					<?php echo validation_errors(); ?>
				</div>
				<?php
			}
			?>
			<form method="post" action="<?php echo base_url(); ?>pesanan/updatepembayaran">
            <input type="hidden" name="id_pesanan" id="id_pesanan" value="<?php echo $pesanan->id_pesanan; ?>"/>

				<div class="form-group">
					<label for="id_pelanggan">Id Pelanggan</label>
					<input type="text" value="<?php echo $pesanan->id_pelanggan; ?>" class="form-control" id="id_pelanggan" name="id_pelanggan">
				</div>

                <div class="form-group">
					<label for="total">Total</label>
					<input type="text" class="form-control" id="total" name="total" value="<?php echo $pesanan->total; ?>">
				</div>

                <div class="form-group">
					<label for="status">Status</label>
					<select name="status" id="status" class="form-control">
						<option value="terbayar" <?php echo ($pesanan->status ? 'terbayar' : 'selected' ); ?> >Terbayar</option>
						<option value="tidak" <?php echo ($pesanan->status ? 'tidak' : 'selected' ); ?>>Tidak</option>
					</select>
				</div>

				<button type="submit" class="btn btn-primary">Bayar</button>
                <a href="<?php echo base_url(); ?>pesanan/index" class="btn btn-success">Batal</a>
			</form>
			<div class="container" style="text-align:center;">
        		<hr></hr>
       				 <span>Aplikasi Penjualan UMKM Apem Kesesi <?php echo date('Y'); ?></span>
       			 <br></br>
     			</div>
			</div>
		</div>
	</div>